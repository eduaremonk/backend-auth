const express = require('express');
const cors = require('cors');
const path = require('path');
const morgan = require('morgan');
const { dbConnection } = require('./database/config');
require('dotenv').config();



// Crear el servidor/aplicacion de express
const app = express();

// DB conection
dbConnection();

// Middleware
app.use(morgan('dev'))

app.use(express.static('public'));
app.use( cors() );

// Lectura y parseo del body
app.use(express.json());

// Rutas

app.use('/api/auth', require('./routes/auth'));

app.listen(process.env.PORT,()=>{
    console.log(`Server Runing in port ${process.env.PORT}`);
});