const { crearUsuario, loginUsuario,revonarToken } = require('../controllers/auth');
const { Router} = require('express');
const { check } = require('express-validator');
const { validarCampos } = require('../middlewares/validar-campos');
const { validarJWT } = require('../middlewares/validar-jwt');


const router = Router();

// Crear un nuevo usuario
router.post('/new',[
    check('name','El nombre es obligatorio').not().isEmpty(),
    check('email','El email es obligatorio').isEmail(),
    check('password','La contraseña es obligatorio').isLength({min:6}),
    validarCampos
],crearUsuario);


// Login usuario
router.post('/',[
    check('email','El email es obligatorio').isEmail(),
    check('password','La contraseña debe tener mas de 6 caracteres').isLength({min:6}),
    validarCampos
],loginUsuario);

// Validar y revalidad Token
router.get('/renew',validarJWT,revonarToken);

module.exports = router;