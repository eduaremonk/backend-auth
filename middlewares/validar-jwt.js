const { response } = require('express');
const jwt = require('jsonwebtoken');

const validarJWT = ( req, res = response, next ) => {
    
    const token = req.header('x-token');
    console.log(token);
    
    if(!token){
        return res.status(401).json({
            ok: false,
            "msg":"Token invalidooo"
        })
    }

    try{
        
       const {uid, name} = jwt.verify(token, process.env.SECRET_JWT_SEED);
       req.uid = uid;
       req.name = name;

    }catch(err){
        console.log(err);
        return res.status(401).json({
            ok: false,
            msg:'Token invalidosss'
        });
    }
    
    next();
}

module.exports = {
    validarJWT,
}
